# Sites for Online Learning

## Courses
1. Coursera
1. edX
1. khan Academy
1. Udemy
1. iTunesU Free Courses
1. MIT opencourceware
1. stanford online
1. codecadamy
1. ict iitr
1. iotiitk
1. [Udacity](https://www.udacity.com/courses/all?price=Free)
1. NEPTEL

## Learn AWS
1. [awseducate.com](https://www.awseducate.com/registration/s/registration-detail)
1. aws.amazon.com/getting-started
1. amazon.qwiklabs.com

## AI
1. [elements of ai](https://www.elementsofai.com/)