# Resources for UI/UX

## Design Inspirations
1. [siteinspire](https://www.siteinspire.com/)
1. [webdesign inspiration](https://www.webdesign-inspiration.com/)
1. [minimal.gallery](https://minimal.gallery/)
1. [mobbin](https://mobbin.com/features)
1. [niceverynice](https://www.niceverynice.com/)
1. [godly.website](https://godly.website/)
1. [lapa.ninja](https://www.lapa.ninja/)
1. [nicelydone](https://nicelydone.club/)
1. [refero.design](https://refero.design/)
1. [land-book](https://land-book.com/)
1. [httpster](https://httpster.net/)
1. [landingfolio](https://www.landingfolio.com/)
1. [pagecollective](https://pagecollective.com/)
1. [pageflows](https://pageflows.com/)
1. [saaslandingpage](https://saaslandingpage.com/)
1. [screen lane](https://screenlane.com/)
1. [uigarage](https://uigarage.net/)
1. [https://luxiren-mui5.vercel.app/en/](https://luxiren-mui5.vercel.app/en/)

## Cool Websites design
1. [https://animecountdown.com/](https://animecountdown.com/): gradient, glassmorphic, animation

## Themes and Pallets
1. [aicolors](https://aicolors.co/): ai, prompt
1. [cheekypalettes](https://www.cheekypalettes.com/)
1. [cssgradient](https://cssgradient.io/)

## Figma Designs
1. [produce-ui](https://produce-ui.com/components.html)
